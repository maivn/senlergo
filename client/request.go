package client

import (
	"bytes"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
)

type HttpClient struct {
	VkGroupId   string `json:"vk_group_id"`
	SecretKey   string `json:"secret_key"`
	AccessToken string `json:"access_token"`
	Version     string `json:"version"`
}

func newHttpClient(vkGroupId, secretKey, accessToken, version string) *HttpClient {
	return &HttpClient{
		VkGroupId:   vkGroupId,
		SecretKey:   secretKey,
		AccessToken: accessToken,
		Version:     version,
	}
}

type OptionsRequest struct {
	Url  url.URL
	Form map[string]string
}

func (h *HttpClient) request(optionsRequest *OptionsRequest, result interface{}) error {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	if optionsRequest.Form != nil {
		for k, v := range optionsRequest.Form {
			field, err := writer.CreateFormField(k)
			if err != nil {
				return err
			}
			_, err = io.Copy(field, strings.NewReader(v))
			if err != nil {
				return err
			}
		}
	}
	writer.Close()

	req, err := http.NewRequest(http.MethodPost, optionsRequest.Url.String(), bytes.NewReader(body.Bytes()))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	switch code := resp.StatusCode; true {
	case code >= 400:
		//todo
	case code == 200:
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		if result != nil {
			err = json.Unmarshal(body, result)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
