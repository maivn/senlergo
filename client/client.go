package client

type Senler struct {
	Subscription Subscription `json:"subscription"`
	Subscriber   Subscriber   `json:"subscriber"`
}

func NewSenler(vkGroupId, secretKey, token string) *Senler {
	httpClient := newHttpClient(vkGroupId, secretKey, token, "2")
	return &Senler{
		Subscription: newSubscription(httpClient),
		Subscriber:   newSubscriber(httpClient),
	}
}
