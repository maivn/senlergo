package client

import (
	"fmt"
	"net/url"
)

type Subscription struct {
	HttpClient *HttpClient
}

func newSubscription(httpClient *HttpClient) Subscription {
	return Subscription{HttpClient: httpClient}
}

type GetSubscriptionResponse struct {
	Success bool   `json:"success"`
	Count   string `json:"count"`
	Items   []struct {
		SubscriptionId string `json:"subscription_id"`
		Name           string `json:"name"`
	}
}

func (s *Subscription) Get(count, offset int64) (result GetSubscriptionResponse, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   "senler.ru",
		Path:   "/api/subscriptions/get",
	}

	form := make(map[string]string)
	form["vk_group_id"] = s.HttpClient.VkGroupId
	form["v"] = s.HttpClient.Version
	form["access_token"] = s.HttpClient.AccessToken
	form["offset"] = fmt.Sprintf("%d", offset)
	if count > 0 && count <= 100 {
		form["count"] = fmt.Sprintf("%d", count)
	}

	options := OptionsRequest{
		Url:  u,
		Form: form,
	}

	err = s.HttpClient.request(&options, &result)
	if err != nil {
		return
	}
	return
}
