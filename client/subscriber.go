package client

import (
	"net/url"
)

type Subscriber struct {
	HttpClient *HttpClient
}

func newSubscriber(httpClient *HttpClient) Subscriber {
	return Subscriber{HttpClient: httpClient}
}

type AddSubscriberResponse struct {
	Success bool `json:"success"`
}

func (s *Subscriber) Add(vkUserId, subscriptionId string) (result AddSubscriberResponse, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   "senler.ru",
		Path:   "/api/subscribers/add",
	}

	form := make(map[string]string)
	form["vk_group_id"] = s.HttpClient.VkGroupId
	form["v"] = s.HttpClient.Version
	form["access_token"] = s.HttpClient.AccessToken
	form["vk_user_id"] = vkUserId
	form["subscription_id"] = subscriptionId

	options := OptionsRequest{
		Url:  u,
		Form: form,
	}

	err = s.HttpClient.request(&options, &result)
	if err != nil {
		return
	}
	return
}

type DelSubscriberResponse struct {
	Success bool `json:"success"`
}

func (s *Subscriber) Del(vkUserId, subscriptionId string) (result DelSubscriberResponse, err error) {
	u := url.URL{
		Scheme: "https",
		Host:   "senler.ru",
		Path:   "/api/subscribers/del",
	}

	form := make(map[string]string)
	form["vk_group_id"] = s.HttpClient.VkGroupId
	form["v"] = s.HttpClient.Version
	form["access_token"] = s.HttpClient.AccessToken
	form["vk_user_id"] = vkUserId
	form["subscription_id"] = subscriptionId

	options := OptionsRequest{
		Url:  u,
		Form: form,
	}
	err = s.HttpClient.request(&options, &result)
	if err != nil {
		return
	}
	return
}
